package com.skyit.tutorial

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.repository.findByIdOrNull

@DataJpaTest
class RepositoriesTest @Autowired constructor(
        val entityManager: TestEntityManager,
        val userRepository: UserRepository,
        val articleRepository: ArticleRepository){

    @Test
    fun `When findByIdOrNull return Article` () {
        val testuser = User("testuser", "Test", "User")
        entityManager.persist(testuser)
        val article = Article("Spring Framework 5.0 goes GA", "Dear Spring community ...", "Lorem ipsum", testuser)
        entityManager.persist(article)
        entityManager.flush()
        val found = articleRepository.findByIdOrNull(article.id!!)
        assertThat(found).isEqualTo(article)
    }

    @Test
    fun `When findByLogin return User`() {
        val testuser = User("testuser", "Test", "User")
        entityManager.persistAndFlush(testuser)
        val found = userRepository.findByLogin(testuser.login)

        assertThat(found).isEqualTo(testuser)

    }
}